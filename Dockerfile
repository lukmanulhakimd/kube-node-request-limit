FROM nginx:1.15.1

RUN set -ex; \
    \
    Deps=" \
        wget \
        ca-certificates \
        supervisor \
        procps \
    "; \
    apt-get update; \
    apt-get install -y --no-install-recommends $Deps; \
    rm -rf /var/lib/apt/lists/*; \
    wget -O /usr/bin/kubectl-v1.10 https://storage.googleapis.com/kubernetes-release/release/v1.10.2/bin/linux/amd64/kubectl; \
    wget -O /usr/bin/kubectl-v1.4 https://storage.googleapis.com/kubernetes-release/release/v1.4.12/bin/linux/amd64/kubectl; \
    chmod +x /usr/bin/kubectl*

COPY kube-node-request-limit.sh /usr/bin/
