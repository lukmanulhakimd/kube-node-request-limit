#!/bin/bash

dstDir=/tmp/data
tmpDir=/tmp/temp
## nodes
NODES=$(kubectl-$KUBE_VERSION get nodes -o jsonpath='{.items[*].metadata.name}')
#NODES="172.16.88.21 172.16.88.22"

mkdir $dstDir $tmpDir || true
while true; do
  for node in $NODES
  do
    kubectl-$KUBE_VERSION describe node $node > $tmpDir/$node
    echo -e "# metrics
  cpu_request `cat $tmpDir/$node | grep  -A3 'Total limits' | tail -1 | awk '{print $1}' | sed 's/[m]//g'`
  cpu_request_percent `cat $tmpDir/$node | grep  -A3 'Total limits' | tail -1 | awk '{print $2}' | sed 's/[()%]//g'`
  memory_request `cat $tmpDir/$node | grep  -A3 'Total limits' | tail -1 | awk '{print $5}' | sed 's/[mMiKk]//g'`
  memory_request_percent `cat $tmpDir/$node | grep  -A3 'Total limits' | tail -1 | awk '{print $6}' | sed 's/[()%]//g'`" > $dstDir/$node
  done
  sleep 300
done
