Build docker image.
```
docker build -t registry.gitlab.com/lukmanulhakimd/kube-node-request-limit:v0.3 .
```

Deploy to kube.
```
kubectl apply -f kube-node-request-limit.yml
```